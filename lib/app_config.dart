import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

class AppConfig {
  AppConfig({@required this.flavorName, @required this.apiBaseUrl});

  final String flavorName;
  final String apiBaseUrl;
}
