import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/db/db_manager.dart';
import 'package:tour_dispatch/model/guest_info_model.dart';
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/viewModel/guest_info_view_model.dart';
import 'package:tour_dispatch/model/viewModel/guests_view_model.dart';
import 'package:tour_dispatch/model/viewModel/tour_sale_view_model.dart';

final GuestManager guestManager = _GuestManager._internal();

class _GuestManager implements GuestManager {
  _GuestManager._internal();

  // Change Notifiers
  GuestsViewModel _guestsViewModel = GuestsViewModel()..loading = true;

  // Listeners
  void _startListeningGuests(BuildContext context) async {
    var dbManager = Provider.of<DatabaseManager>(context);
    await for (var guest in dbManager.getGuests(context)) {
      if (_guestsViewModel.isDisposed()) break;
      _guestsViewModel.addGuest(guest);
    }
  }

  // Straightly getters
  @override
  Future<GuestModel> getGuestById(BuildContext context, int id) async {
    var dbManager = Provider.of<DatabaseManager>(context);
    return await dbManager.getGuestById(id);
  }

  // Providers
  @override
  ChangeNotifierProvider<GuestsViewModel> getGuestsNotifier(
      BuildContext context, Widget child) {
    if (_guestsViewModel.isDisposed()) {
      _guestsViewModel = GuestsViewModel()..loading = true;
    }
    _startListeningGuests(context);

    return ChangeNotifierProvider<GuestsViewModel>.value(
      notifier: _guestsViewModel,
      child: child,
    );
  }

  // Processors
  @override
  void confirmGuest(BuildContext context, int id) {
    var dbManager = Provider.of<DatabaseManager>(context);
    dbManager.updateGuest(context, id, DateTime.now(), true);
  }

  @override
  Future<void> markAllNotBoarded(BuildContext context) async {
    var dbManager = Provider.of<DatabaseManager>(context);
    await dbManager.markAllNotBoarded(context);
  }

  @override
  void getFamilyMembers(
      BuildContext context, TourSaleViewModel tourSaleViewModel) async {
    var dbManager = Provider.of<DatabaseManager>(context);
    await Future.delayed(const Duration(milliseconds: 500));
    var guestId = tourSaleViewModel.guest.id;
    var guests = dbManager.getFamilyMembers(guestId);
    guests[guestId] = tourSaleViewModel.guest.name;
    tourSaleViewModel.availableGuests = guests;
  }

  @override
  void addGuestsToTour(BuildContext context, Map<int, String> guests) {
    var dbManager = Provider.of<DatabaseManager>(context);
    guests.forEach((id, name) {
      dbManager.addGuestToTour(context, id, name);
    });
  }

  @override
  ChangeNotifierProvider<GuestInfoViewModel> getGuestInfoById(
      BuildContext context, int id, Widget child) {
    _getGuestsInfo(context, id);
    return ChangeNotifierProvider<GuestInfoViewModel>(
      builder: (_) => _guestsInfoModel,
      child: child,
    );
  }

  var _guestsInfoModel = GuestInfoViewModel()..loading = true;

  Future<GuestInfoModel> _getGuestsInfo(BuildContext context, int id) async {
    var dbManager = Provider.of<DatabaseManager>(context);
    if (_guestsInfoModel.isDisposed()) {
      _guestsInfoModel = GuestInfoViewModel()..loading = true;
    }

    var guestInfo = await dbManager.getGuestInfoById(id);
    return _guestsInfoModel.guestInfo = guestInfo;
  }
}

abstract class GuestManager {
  ChangeNotifierProvider<GuestsViewModel> getGuestsNotifier(
      BuildContext context, Widget child);

  Future<void> markAllNotBoarded(BuildContext context);

  Future<GuestModel> getGuestById(BuildContext context, int id);

  ChangeNotifierProvider<GuestInfoViewModel> getGuestInfoById(
      BuildContext context, int id, Widget child);

  void confirmGuest(BuildContext context, int id);

  void getFamilyMembers(
      BuildContext context, TourSaleViewModel tourSaleViewModel);

  void addGuestsToTour(BuildContext context, Map<int, String> guests);
}
