import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/app_config.dart';
import 'package:tour_dispatch/login/globals.dart' as globals;
import 'package:tour_dispatch/model/excursion_model.dart';
import 'package:tour_dispatch/model/guest_info_model.dart';
import 'package:tour_dispatch/model/guest_model.dart';

const Map<int, GuestInfoModel> GUESTS_INFO = const {
  9817230912: const GuestInfoModel(
      id: 9817230912,
      name: "Isaac Asimov",
      phoneNumber: "555 222 3333",
      cruiseLine: "Holland American Line",
      excursions: [
        ExcursionModel("Juneau Culinary Walk", "Boarded at 11:25AM", true)
      ]),
  3610223761: const GuestInfoModel(
      id: 3610223761,
      name: "Robert Chase",
      phoneNumber: "555 333 1111",
      cruiseLine: "Holland American Line",
      excursions: [
        ExcursionModel("Juneau Culinary Walk", "Boarded at 11:25AM", true)
      ]),
  102010202: const GuestInfoModel(
      id: 102010202,
      name: "Elijah Bailey",
      phoneNumber: "555 444 1111",
      cruiseLine: "Holland American Line",
      excursions: [
        ExcursionModel("Juneau Culinary Walk", "Boarded at 11:25AM", true),
        ExcursionModel("Bear Watching and Wildlife Viewing Adventure",
            "Boarded at 2:12PM", true),
        ExcursionModel("Dog Sleding on Mendenhall Glacier", "Reserved", false),
      ]),
};

class FDatabaseManager implements DatabaseManager {
  FDatabaseManager.internal();

  /// Stream all the Guests
  Stream<GuestModel> getGuests(BuildContext context) async* {
    var appConfig = Provider.of<AppConfig>(context);

    if (appConfig.flavorName == "Development") {
      await for (var data
          in Firestore.instance.collection('status').snapshots()) {
        for (var doc in data.documents) {
          var boardingTime = doc["boarded"] == true
              ? DateTime.parse(doc["boardingTime"])
              : null;

          yield GuestModel()
            ..id = doc["id"]
            ..name = doc["name"]
            ..boarded = doc["boarded"]
            ..boardingTime = boardingTime;
        }
      }
    } else {
      Uri uri = new Uri.http(appConfig.apiBaseUrl, "/guests");
      Response response = await globals.httpClient.get(uri);
      Iterable guestsJson = json.decode(response.body);
      for (var guest in guestsJson) {
        GuestModel guestModel = GuestModel.fromJson(guest);
        yield guestModel;
      }
    }
  }

  Future<GuestModel> getGuestById(int id) async {
    print("retrieving $id");
    var doc =
        await Firestore.instance.collection('guests').document('$id').get();

    if (doc == null) {
      return GuestModel()..id = 0;
    }

    return GuestModel()
      ..id = doc["id"]
      ..name = doc["name"]
      ..room = doc["room"];
  }

  Map<int, String> getFamilyMembers(int id) {
    return {1: "Kate Cohen", 2: "Matt Cohen", 3: "Thomas Cohen"};
  }

  /// Set boarded to the value of [boarded] for the Guest with the given [id]
  Future<void> updateGuest(
      BuildContext context, int id, DateTime boardingTime, bool boarded) async {
    var appConfig = Provider.of<AppConfig>(context);
    Uri uri = new Uri.http(appConfig.apiBaseUrl, "/guests");
    globals.httpClient
        .put(uri)
        .then((v) => print("Updated"))
        .catchError((error) {
      Firestore.instance.collection('status').document('$id').updateData(
          {'boardingTime': boardingTime.toString(), 'boarded': boarded});
    });
  }

  /// Get a snapshot of the DB and mark all the Guests as not boarded
  Future<void> markAllNotBoarded(BuildContext context) async {
    var snapshot =
        await Firestore.instance.collection('status').snapshots().first;
    snapshot.documents.forEach((doc) {
      updateGuest(context, doc["id"], null, false);
    });
  }

  Future<void> addGuestToTour(BuildContext context, int id, String name) async {
    var appConfig = Provider.of<AppConfig>(context);
    Uri uri = new Uri.http(appConfig.apiBaseUrl, "/guests");
    globals.httpClient
        .post(uri)
        .then((v) => print("Saved"))
        .catchError((error) async {
      await Firestore.instance.collection('status').document('$id').setData({
        'boarded': true,
        'boardingTime': DateTime.now().toString(),
        'cabin': 202,
        'id': id,
        'name': name
      });
    });
  }

  @override
  Future<GuestInfoModel> getGuestInfoById(int id) async {
    await Future.delayed(const Duration(seconds: 1));
    return GUESTS_INFO[id];
  }
}

abstract class DatabaseManager {
  Stream<GuestModel> getGuests(BuildContext context);

  Map<int, String> getFamilyMembers(int id);

  Future<GuestModel> getGuestById(int id);

  Future<GuestInfoModel> getGuestInfoById(int id);

  Future<void> updateGuest(
      BuildContext context, int id, DateTime boardingTime, bool boarded);

  Future<void> markAllNotBoarded(BuildContext context);

  Future<void> addGuestToTour(BuildContext context, int id, String name);
}
