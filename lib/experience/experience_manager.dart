import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/model/experience_model.dart';
import 'package:tour_dispatch/model/viewModel/experiences_view_model.dart';

final ExperienceManager experienceManager = _MockExperienceManager._internal();

const Map<int, ExperienceModel> EXPERIENCES = const {
  0: const ExperienceModel(0, "Alaska & The Yukon", 10,
      "assets/alaska-banner.jpg", true, "1 hour", 90, 65),
  1: const ExperienceModel(1, "Whale Watching, Mendenhall Glacier", 0,
      "assets/whale-banner.png", false, "30 minutes", 50, 30),
  2: const ExperienceModel(2, "Juneau Culinary Walk", 0,
      "assets/juneau-banner.png", false, "2 hour", 100, 80),
};

class _MockExperienceManager implements ExperienceManager {
  _MockExperienceManager._internal();

  var _experiencesModel = ExperiencesViewModel()..loading = true;

  void _getExperiences() async {
    if (_experiencesModel.isDisposed()) {
      _experiencesModel = ExperiencesViewModel()..loading = true;
    }

    await Future.delayed(const Duration(seconds: 1));

    return _experiencesModel.addExperiences(EXPERIENCES);
  }

  @override
  ChangeNotifierProvider<ExperiencesViewModel> getExperiences(Widget child) {
    _getExperiences();
    return ChangeNotifierProvider<ExperiencesViewModel>(
      builder: (_) => _experiencesModel,
      child: child,
    );
  }
}

abstract class ExperienceManager {
  ChangeNotifierProvider<ExperiencesViewModel> getExperiences(Widget child);
}
