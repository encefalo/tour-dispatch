import 'dart:async';
import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/app_config.dart';
import 'package:tour_dispatch/login/globals.dart' as globals;
import 'package:tour_dispatch/model/user_model.dart';

final LoginManager loginManager = _MockLoginManager._internal();

const Map<String, UserModel> USERS = const {
  "miguel@globant.com": const UserModel(
      "Miguel Castiblanco", "miguel@globant.com", "assets/miguel-profile.jpg"),
  "martin@globant.com": const UserModel(
      "Martin Uribe", "martin@globant.com", "assets/martin-profile.png"),
  "rodrigo@globant.com": const UserModel(
      "Rodrigo Rivas", "rodrigo@globant.com", "assets/rodrigo-profile.jpeg"),
  "juan@globant.com": const UserModel(
      "Juan Mena", "juan@globant.com", "assets/juan-profile.jpeg"),
  "berco@globant.com": const UserModel(
      "Gerardo Bercovich", "berco@globant.com", "assets/berco-profile.jpg"),
  "brendan@hal.com":
      const UserModel("Brendan Schenk", "brendan@hal.com", "assets/default.png")
};

class _MockLoginManager implements LoginManager {
  _MockLoginManager._internal();

  UserModel _currentUser;

  Future<bool> login(BuildContext context, String username, String password) async {
    return loginRest(context,username, password)
        .then((user) {
          _currentUser = user;
          return true;
        })
        .catchError((error) {
           _currentUser = USERS[username];
           return (_currentUser != null && password == "123456") ? true : false;
        });
  }

  Future<UserModel> loginRest(BuildContext context, String username, String password) async {
    var appConfig = Provider.of<AppConfig>(context);
    Uri uri = new Uri.http(appConfig.apiBaseUrl, "/login");
    return globals.httpClient.get(uri).then((response) {
      return UserModel.fromJson(json.decode(response.body));
    });
  }

  UserModel getCurrentUser() => _currentUser;
}

abstract class LoginManager {
  Future<bool> login(BuildContext context, String username, String password);
  UserModel getCurrentUser();
}
