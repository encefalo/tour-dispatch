import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'provider_setup.dart';
import 'ui/route/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: providers,
        child: MaterialApp(
          title: "${Strings.APP_NAME}",
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Login(),
        ));
  }
}
