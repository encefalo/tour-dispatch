import 'package:flutter/foundation.dart';

/// ChangeNotifier to handle commons functionalities
abstract class BaseChangeNotifier extends ChangeNotifier {
  bool _disposed = false;

  @override
  void dispose() {
    super.dispose();
    _disposed = true;
  }

  bool isDisposed() => _disposed;
}