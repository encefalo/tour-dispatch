class ExcursionModel {
  final String name;
  final String state;
  final bool boarded;

  const ExcursionModel(this.name, this.state, this.boarded);
}
