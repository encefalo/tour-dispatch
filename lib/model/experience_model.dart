class ExperienceModel {
  final int id;
  final String name;
  final int reservations;
  final String imagePath;
  final bool isEnable;
  final String duration;
  final double adultCost;
  final double childCost;

  const ExperienceModel(this.id, this.name, this.reservations, this.imagePath,
      this.isEnable, this.duration, this.adultCost, this.childCost);
}
