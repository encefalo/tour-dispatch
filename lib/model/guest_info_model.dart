import 'excursion_model.dart';

class GuestInfoModel {
  final int id;
  final String name;
  final String phoneNumber;
  final String cruiseLine;
  final List<ExcursionModel> excursions;

  const GuestInfoModel(
      {this.id, this.name, this.phoneNumber, this.cruiseLine, this.excursions});
}
