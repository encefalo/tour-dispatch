import 'package:json_annotation/json_annotation.dart';

part 'guest_model.g.dart';

@JsonSerializable()
class GuestModel {
  int id;
  String name;
  bool boarded;
  DateTime boardingTime;
  bool alreadyOnTour;
  String room;

  GuestModel(
      {this.id,
      this.name,
      this.boarded,
      this.boardingTime,
      this.alreadyOnTour,
      this.room});

  factory GuestModel.fromJson(Map<String, dynamic> json) =>
      _$GuestModelFromJson(json);

  Map<String, dynamic> toJson() => _$GuestModelToJson(this);

  @override
  String toString() {
    return 'GuestModel{id: $id, name: $name, boarded: $boarded, ' +
        'boardingTime: $boardingTime, alreadyOnTour: $alreadyOnTour, ' +
        'room: $room}';
  }
}
