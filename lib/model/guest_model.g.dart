// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'guest_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GuestModel _$GuestModelFromJson(Map<String, dynamic> json) {
  return GuestModel(
      id: json['id'] as int,
      name: json['name'] as String,
      boarded: json['boarded'] as bool,
      boardingTime: json['boardingTime'] == null
          ? null
          : DateTime.parse(json['boardingTime'] as String),
      alreadyOnTour: json['alreadyOnTour'] as bool);
}

Map<String, dynamic> _$GuestModelToJson(GuestModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'boarded': instance.boarded,
      'boardingTime': instance.boardingTime?.toIso8601String(),
      'alreadyOnTour': instance.alreadyOnTour
    };
