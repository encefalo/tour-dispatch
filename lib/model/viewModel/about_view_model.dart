import 'package:tour_dispatch/model/about_model.dart';
import 'package:tour_dispatch/model/viewModel/base_view_model.dart';

class AboutViewModel extends BaseViewModel {
  final AboutModel _about = AboutModel();

  AboutModel get about => _about;

  void setAbout(String name, String version) {
    _about.version = version;
    _about.name = name;
    notifyListeners();
  }
}
