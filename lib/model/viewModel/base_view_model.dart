import 'package:tour_dispatch/model/base_change_notifier.dart';

/// Implements commons properties of ViewModels like loading
abstract class BaseViewModel extends BaseChangeNotifier {
  bool _loading = false;

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }
}
