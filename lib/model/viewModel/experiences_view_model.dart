import '../experience_model.dart';
import 'base_view_model.dart';

class ExperiencesViewModel extends BaseViewModel {
  final Map<int, ExperienceModel> _experiences = Map<int, ExperienceModel>();

  Map<int, ExperienceModel> get experiences => _experiences;

  void addExperiences(Map<int, ExperienceModel> newExperiences) {
    _experiences.addAll(newExperiences);
    notifyListeners();
  }
}
