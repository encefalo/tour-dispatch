import '../guest_info_model.dart';
import 'base_view_model.dart';

class GuestInfoViewModel extends BaseViewModel {
  GuestInfoModel _guestInfo;

  GuestInfoModel get guestInfo => _guestInfo;

  set guestInfo(GuestInfoModel guestInfo) {
    _guestInfo = guestInfo;
    notifyListeners();
  }
}
