import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/viewModel/base_view_model.dart';

class GuestsViewModel extends BaseViewModel {
  final Map<int, GuestModel> _guests = Map<int, GuestModel>();

  Map<int, GuestModel> get guests => _guests;

  void addGuest(GuestModel guest) {
    _guests[guest.id] = guest;
    notifyListeners();
  }
}