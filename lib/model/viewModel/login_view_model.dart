
import 'package:tour_dispatch/model/viewModel/base_view_model.dart';

class LoginViewModel extends BaseViewModel {
  String _email;
  String _password;

  String get password => _password;

  set password(String value) {
    _password = value;
    notifyListeners();
  }

  String get email => _email;

  set email(String value) {
    _email = value;
    notifyListeners();
  }
}
