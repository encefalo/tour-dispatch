import '../experience_model.dart';
import '../guest_model.dart';
import 'base_view_model.dart';

class TourSaleViewModel extends BaseViewModel {
  final Map<int, String> _departures = {
    0: '8:30 AM',
    1: '11:00 AM',
    2: '2:30 PM',
  };

  GuestModel guest;
  ExperienceModel experienceModel;
  String _selectedTime;
  int _selectedTimeIndex = -1;
  Map<int, String> _availableGuests = {};
  Map<int, String> _selectedGuests = {};

  Map<int, String> get departures => _departures;

  Map<int, String> get availableGuests => _availableGuests;

  Map<int, String> get selectedGuests => _selectedGuests;

  String get selectedTime => _selectedTime;

  int get selectedTimeIndex => _selectedTimeIndex;

  set selectedTimeIndex(int value) {
    _selectedTimeIndex = value;
    _selectedTime = departures[value];
    notifyListeners();
  }

  set availableGuests(Map<int, String> value) {
    _availableGuests = value;
    notifyListeners();
  }

  void selectGuest(int id) {
    selectedGuests[id] = availableGuests[id];
    notifyListeners();
  }

  void removeGuest(int id) {
    selectedGuests.remove(id);
    notifyListeners();
  }

  // Since this ViewModel is used over many screens dispose only must be performed
  // if there is no listeners left
  @override
  void dispose() {
    if (!hasListeners) super.dispose();
  }
}
