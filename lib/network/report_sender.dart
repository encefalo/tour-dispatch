import 'package:simple_http_client/simple_http_client.dart';
import 'dart:io';

var _client = new SimpleHttpClient(userAgent: "Tour Dispatcher");

Future<bool> sendReport() async {
  var response = await _client.get(
      "https://us-central1-tour-dispatcher.cloudfunctions.net/testEmail");

  return response.statusCode == HttpStatus.ok;
}
