// provider_setup.dart
import 'package:provider/provider.dart';

import 'app_config.dart';
import 'db/db_manager.dart';

List<SingleChildCloneableWidget> providers = [
  ...independentServices,
  ...dependentServices,
  ...uiConsumableProviders,
];

List<SingleChildCloneableWidget> independentServices = [
  Provider.value(value: AppConfig(flavorName: "Production",apiBaseUrl: "prod:8081")),
  Provider<DatabaseManager>.value(value: FDatabaseManager.internal()),
];

List<SingleChildCloneableWidget> dependentServices = [];

List<SingleChildCloneableWidget> uiConsumableProviders = [];


// Dev environment
List<SingleChildCloneableWidget> providersDev = [
  ...independentServicesDev,
  ...dependentServicesDev,
  ...uiConsumableProvidersDev,
];

List<SingleChildCloneableWidget> independentServicesDev = [
  Provider.value(value: AppConfig(flavorName: "Development",apiBaseUrl: "dev:8081")),
  Provider<DatabaseManager>.value(value: FDatabaseManager.internal()),
];

List<SingleChildCloneableWidget> dependentServicesDev = [];

List<SingleChildCloneableWidget> uiConsumableProvidersDev = [];