import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/model/viewModel/about_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

class AboutInfo extends StatelessWidget {
  final _aboutViewModel = AboutViewModel();

  AboutInfo() {
    _retrieveAboutName();
  }

  _retrieveAboutName() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _aboutViewModel.setAbout(packageInfo.appName, packageInfo.version);
  }

  @override
  Widget build(BuildContext context) {
    var body = Consumer<AboutViewModel>(builder: (context, aboutViewModel, _) => Column(children: [
      Container(
          margin: const EdgeInsets.all(16),
          child: SizedBox(
              width: double.infinity,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 4.0,
                  child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(bottom: 8),
                                child: Text(
                                  aboutViewModel.about.name,
                                  style: Theme.of(context).textTheme.title,
                                )),
                            Text(
                              aboutViewModel.about.version,
                              style: Theme.of(context).textTheme.body1,
                            ),
                          ]))))),
    ]));
    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.ABOUT_TITLE),
        ),
        body: ChangeNotifierProvider<AboutViewModel>(
          builder: (_) => _aboutViewModel,
          child: body,
        ));
  }
}
