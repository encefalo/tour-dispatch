import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/model/experience_model.dart';
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/ui/widget/guests_list.dart';
import 'package:tour_dispatch/utils/strings.dart';

import '../../utils/dialogs.dart';
import 'guest_info.dart';

class BoardingStatus extends StatelessWidget {
  static final _scaffoldKey = GlobalKey<ScaffoldState>();
  static const ROUTE = "/boarding_status";
  static final _scanButtonKey = Key("scan_button_key");

  _scan(Map<int, GuestModel> guests, ExperienceModel experienceModel,
      BuildContext context) async {
    try {
      int id = int.parse(await BarcodeScanner.scan());
      var guestModel = guests[id];

      // Only if a guest is not in the current list is obtained from fb
      if (guestModel == null) {
        guestModel = await guestManager.getGuestById(context, id)
          ..alreadyOnTour = false
          ..boarded = false;
      } else {
        guestModel.alreadyOnTour = true;
      }

      Navigator.push(
          context,
          MaterialPageRoute<GuestInfo>(
              builder: (_) => MultiProvider(
                    providers: [
                      Provider<GuestModel>(builder: (_) => guestModel),
                      Provider<ExperienceModel>(builder: (_) => experienceModel)
                    ],
                    child: GuestInfo(),
                  )));
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        showError(Strings.CAMERA_PERMISSIONS_ERROR, context);
      } else {
        showError(Strings.BARCODE_SCANNING_ERROR, context);
      }
    } catch (e) {
      // ignore when the user presses back without scanning
      if (!(e is FormatException)) {
        showError(Strings.BARCODE_SCANNING_ERROR, context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var guestsNotifier = guestManager.getGuestsNotifier(context, GuestsList());
    var experienceModel = Provider.of<ExperienceModel>(context);

    return Scaffold(
        key: _scaffoldKey,
        floatingActionButton: Builder(builder: (BuildContext context) {
          return FloatingActionButton(onPressed: () =>
            _scan(guestsNotifier.value.guests, experienceModel, context),
            child: const Icon(Icons.camera),
            key: _scanButtonKey
          );
        }),
        body: guestsNotifier);
  }
}
