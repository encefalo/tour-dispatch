import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/model/experience_model.dart';
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/viewModel/tour_sale_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'tour_sale.dart';

class GuestInfo extends StatelessWidget {
  static final _confirmedButtonKey = Key("confirmed_button_key");
  static final _startSaleButtonKey = Key("start_sale_button_key");

  _confirm(GuestModel guestModel, BuildContext context) {
    guestManager.confirmGuest(context, guestModel.id);
    Navigator.pop(context);
  }

  _continue(GuestModel guestModel, ExperienceModel experienceModel,
      BuildContext context) {
    Navigator.push(context, MaterialPageRoute<TourSale>(builder: (_) {
      var tourSaleModel = TourSaleViewModel()
        ..guest = guestModel
        ..experienceModel = experienceModel;

      return ChangeNotifierProvider<TourSaleViewModel>(
        builder: (_) => tourSaleModel,
        child: TourSale(),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    var guestModel = Provider.of<GuestModel>(context);
    var experienceModel = Provider.of<ExperienceModel>(context);

    var tourInfo = !guestModel.alreadyOnTour
        ? Container()
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(bottom: 6, top: 28),
                    child: Text(
                      experienceModel.name,
                      style: Theme.of(context).textTheme.title,
                    )),
                Container(
                    margin: const EdgeInsets.only(bottom: 6),
                    child: Text(
                      Strings.TODAY,
                      style: Theme.of(context).textTheme.body1,
                    )),
                Text(
                  "Departure Time: 8:30 AM",
                  style: Theme.of(context).textTheme.body1,
                )
              ]);

    var offerConfirm = !guestModel.alreadyOnTour || guestModel.boarded
        ? Container()
        : Container(
            margin: EdgeInsets.all(16),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(
                color: Colors.green,
                textColor: Colors.white,
                onPressed: () => _confirm(guestModel, context),
                child: Text(Strings.CONFIRM),
                key: _confirmedButtonKey,
              ),
            ));

    var offerPurchase = guestModel.alreadyOnTour
        ? Container()
        : Container(
            padding: const EdgeInsets.all(28),
            child: Column(children: <Widget>[
              Container(
                  child: Text(
                Strings.GUEST_NOT_REGISTERED,
                style: Theme.of(context).textTheme.title,
              )),
              Container(
                  margin: const EdgeInsets.only(top: 12, bottom: 12),
                  child: Text(Strings.OFFER_TOUR_TO_SAME_ROOM_GUESTS,
                      style: Theme.of(context).textTheme.body1,
                      textAlign: TextAlign.center)),
              Container(
                  margin: EdgeInsets.all(22),
                  child: SizedBox(
                    width: double.infinity,
                    child: OutlineButton(
                      borderSide: BorderSide(
                        color: Colors.blue,
                        style: BorderStyle.solid,
                      ),
                      textColor: Colors.blue,
                      onPressed: () =>
                          _continue(guestModel, experienceModel, context),
                      child: Text(
                        Strings.START_A_SALE,
                        key: _startSaleButtonKey,
                      ),
                    ),
                  ))
            ]));

    var alreadyProcessedContainer = guestModel.boarded
        ? Container(
            padding: const EdgeInsets.all(28),
            child: Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    Strings.ALREADY_PROCESSED,
                    style: Theme.of(context).textTheme.title,
                  ),
                ),
                RichText(
                  text: TextSpan(
                    style: Theme.of(context).textTheme.body1,
                    children: <TextSpan>[
                      TextSpan(text: Strings.RESERVATION_APPEARS_AS),
                      TextSpan(
                          text: Strings.CLAIMED,
                          style: TextStyle(fontStyle: FontStyle.italic)),
                      TextSpan(text: Strings.IN_OUR_SYSTEM),
                    ],
                  ),
                )
              ],
            ),
          )
        : Container();

    var body = Column(children: [
      Container(
          margin: const EdgeInsets.all(16),
          child: SizedBox(
              width: double.infinity,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 4.0,
                  child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(bottom: 8),
                                child: Text(
                                  guestModel.name,
                                  style: Theme.of(context).textTheme.title,
                                )),
                            Text(
                              Strings.CRUISE_LINE,
                              style: Theme.of(context).textTheme.caption,
                            ),
                            Text(
                              "Holland America Line", //TODO: Where does this field come from?
                              style: Theme.of(context).textTheme.body1,
                            ),
                            tourInfo
                          ]))))),
      alreadyProcessedContainer,
      Expanded(child: offerPurchase),
      offerConfirm
    ]);

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.GUEST_INFO),
        ),
        body: body);
  }
}
