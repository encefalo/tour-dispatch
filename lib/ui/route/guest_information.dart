import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/model/viewModel/guest_info_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

class GuestInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var guestInfoVM = Provider.of<GuestInfoViewModel>(context)..loading = true;

    var subtitle = Theme.of(context)
        .textTheme
        .caption
        .copyWith(fontSize: 14, fontWeight: FontWeight.bold);

    if (guestInfoVM.guestInfo != null) {
      guestInfoVM.loading = false;
    }

    var experiences = guestInfoVM.loading
        ? Container(
            height: double.infinity,
            child: Center(child: CircularProgressIndicator()))
        : ListView.separated(
            padding: const EdgeInsets.all(8.0),
            itemCount: guestInfoVM.loading
                ? 0
                : guestInfoVM.guestInfo.excursions.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child: Text(guestInfoVM.guestInfo.excursions[index].name,
                          style: TextStyle(fontSize: 14)),
                    ),
                    Expanded(
                      flex: 5,
                      child: Text(guestInfoVM.guestInfo.excursions[index].state,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 12,
                              color: guestInfoVM
                                      .guestInfo.excursions[index].boarded
                                  ? Colors.red
                                  : Colors.blue)),
                    ),
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(
                  height: 22,
                ),
          );

    var info = guestInfoVM.loading
        ? Container(
            height: double.infinity,
            child: Center(child: CircularProgressIndicator()))
        : Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(children: [
              Row(children: <Widget>[
                Container(
                  padding:
                      const EdgeInsets.only(top: 20, bottom: 20, right: 20),
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: Text(
            guestInfoVM.guestInfo.name,
                    style: TextStyle(
                      fontSize: 38,
                      fontWeight: FontWeight.normal,
                    ),
                    overflow: TextOverflow.clip,
                  ),
                )
              ]),
              Divider(
                height: 20,
              ),
              Container(
                  child: Row(
                children: <Widget>[
                  Icon(
                    Icons.call,
                    color: Colors.black54,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(Strings.CONTACT, style: subtitle),
                  ),
                ],
              )),
              Container(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Row(children: <Widget>[
                    Text(guestInfoVM.guestInfo.phoneNumber,
                        style: new TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline)),
                  ])),
              Container(
                  child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.directions_boat,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Text(
                        Strings.CRUISE_LINE,
                        style: subtitle,
                      ),
                    ),
                  ],
                ),
              )),
              Container(
                  child: Row(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(guestInfoVM.guestInfo.cruiseLine),
                ),
              ])),
            ]));

    var body = Container(
        child: Column(
      children: <Widget>[
        info,
        Container(
            padding: const EdgeInsets.only(left: 16.0, right: 10.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.map,
                  color: Colors.black54,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(Strings.EXCURSIONS, style: subtitle),
                ),
              ],
            )),
        Expanded(
          child: experiences,
        )
      ],
    ));

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.GUEST_INFORMATION),
        ),
        body: guestInfoVM.loading
            ? Container(
                height: double.infinity,
                child: Center(child: CircularProgressIndicator()))
            : body);
  }
}
