import 'package:flutter/material.dart';
import 'package:tour_dispatch/experience/experience_manager.dart';
import 'package:tour_dispatch/ui/widget/experience.dart';
import 'package:tour_dispatch/utils/strings.dart';

import '../widget/drawer.dart';

class Home extends StatelessWidget {
  static final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var experiencesProvider =
        experienceManager.getExperiences(ExperiencesList());

    return Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(child: HALDrawer()),
        appBar: AppBar(
          title: Text(Strings.HOME_TITLE),
        ),
        body: experiencesProvider);
  }
}
