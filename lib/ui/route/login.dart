import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tour_dispatch/ui/route/about_info.dart';
import 'package:tour_dispatch/ui/widget/login_form.dart';
import 'package:tour_dispatch/utils/strings.dart';

class Login extends StatelessWidget {
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    showVersion() {
      Navigator.push(
          context,
          MaterialPageRoute<AboutInfo>(
              builder: (BuildContext context) =>
                  AboutInfo()));
    }
    var body = Stack(
      children: <Widget>[
        SizedBox.expand(
            child: Image.asset(
          'assets/signInBG.png',
          fit: BoxFit.cover,
        )),
        ListView(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.fromLTRB(16, 60, 16, 16),
              child: SvgPicture.asset("assets/tourDispatcherLogoSVG.svg"),
            ),
            LoginForm(),
            SizedBox(
                width: double.infinity,
                child: FlatButton(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: showVersion,
                    child: Text(Strings.ABOUT)))
          ],
        ),
      ],
    );

    return Scaffold(key: _scaffoldKey, body: body);
  }
}
