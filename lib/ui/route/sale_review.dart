import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/model/viewModel/tour_sale_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'boarding_status.dart';

class SaleReview extends StatelessWidget {
  static final _confirmButtonKey = Key("confirm_button");

  void _confirm(TourSaleViewModel tourSaleViewModel, BuildContext context) {
    guestManager.addGuestsToTour(context, tourSaleViewModel.selectedGuests);
    Navigator.popUntil(context, ModalRoute.withName(BoardingStatus.ROUTE));
  }

  @override
  Widget build(BuildContext context) {
    var tourSaleViewModel = Provider.of<TourSaleViewModel>(context);

    var tourInfo = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(bottom: 6, top: 16),
              child: Text(
                tourSaleViewModel.experienceModel.name,
                style: Theme.of(context).textTheme.title,
              )),
          Container(
              margin: const EdgeInsets.only(bottom: 6),
              child: Text(
                Strings.TODAY,
                style: Theme.of(context).textTheme.body1,
              )),
          Container(
              margin: const EdgeInsets.only(bottom: 6),
              child: Text(
                "${Strings.DEPARTURE_TIME} ${tourSaleViewModel.selectedTime}",
                style: Theme.of(context).textTheme.body1,
              )),
          Text(
            "${Strings.NUMBER_OF_GUESTS} ${tourSaleViewModel.selectedGuests.length}",
            style: Theme.of(context).textTheme.body1,
          )
        ]);

    var names = [];
    for (var name in tourSaleViewModel.selectedGuests.values) {
      names.add(Container(
          margin: const EdgeInsets.only(bottom: 24, left: 8),
          child: Text(
            name,
            style: Theme.of(context).textTheme.subhead,
          )));
    }

    var departureTimes = SizedBox(
        width: double.infinity,
        child: Container(
            margin: const EdgeInsets.all(24),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        Strings.GUESTS,
                        style: Theme.of(context).textTheme.title,
                      )),
                  ...names,
                  Divider(color: Colors.blueGrey),
                  Container(
                      margin: const EdgeInsets.only(bottom: 6, top: 24),
                      child: Text(
                        "${Strings.TOTAL} \$${90 * names.length}", //TODO: Calculate
                        style: Theme.of(context).textTheme.headline,
                      )),
                  Text(
                    "${Strings.CHARGE_TO_ROOM} ${tourSaleViewModel.guest.room}",
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ])));

    var offerConfirm = Container(
        margin: EdgeInsets.all(16),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton(
            color: Colors.green,
            textColor: Colors.white,
            onPressed: () => _confirm(tourSaleViewModel, context),
            child: Text(Strings.CONFIRM),
            key: _confirmButtonKey,
          ),
        ));

    var body = Column(children: [
      Container(
          margin: const EdgeInsets.all(16),
          child: SizedBox(
              width: double.infinity,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 4.0,
                  child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[tourInfo]))))),
      Expanded(child: departureTimes),
      offerConfirm
    ]);

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.REVIEW),
        ),
        body: body);
  }
}
