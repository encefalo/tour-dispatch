import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/model/viewModel/tour_sale_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'sale_review.dart';

class SelectGuests extends StatelessWidget {
  static final _continueButtonKey = Key("continue_button");

  void _continue(TourSaleViewModel tourSaleViewModel, BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute<SaleReview>(
            builder: (_) => Provider<TourSaleViewModel>(
                  builder: (_) => tourSaleViewModel,
                  child: SaleReview(),
                )));
  }

  void _onValueChanged(
      int id, TourSaleViewModel tourSaleViewModel, bool selected) {
    if (selected)
      tourSaleViewModel.selectGuest(id);
    else
      tourSaleViewModel.removeGuest(id);
  }

  @override
  Widget build(BuildContext context) {
    var tourSaleViewModel = Provider.of<TourSaleViewModel>(context)
      ..loading = true;

    if (tourSaleViewModel.availableGuests.isEmpty)
      guestManager.getFamilyMembers(context, tourSaleViewModel);

    var friends = <Widget>[];
    tourSaleViewModel.availableGuests.forEach((id, name) {
      tourSaleViewModel.loading = false;
      friends.add(
        CheckboxListTile(
          value: tourSaleViewModel.selectedGuests.containsKey(id),
          onChanged: (selected) =>
              _onValueChanged(id, tourSaleViewModel, selected),
          title: Text(name),
          key: Key("guest_checkbox_${friends.length}"),
        ),
      );
    });

    var guests = SizedBox(
        width: double.infinity,
        child: Container(
            margin: const EdgeInsets.all(24),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        Strings.WHO_IS_GOING,
                        style: Theme.of(context).textTheme.title,
                      )),
                  Container(
                      margin: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        "${Strings.LIST_OF_GUESTS} ${tourSaleViewModel.guest.room}",
                        style: Theme.of(context).textTheme.body1,
                      )),
                  ...friends
                ])));

    var continueButton = Container(
        margin: EdgeInsets.all(16),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton(
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: tourSaleViewModel.selectedGuests.isNotEmpty
                ? () => _continue(tourSaleViewModel, context)
                : null,
            child: Text(Strings.CONTINUE),
            key: _continueButtonKey,
          ),
        ));

    var body = tourSaleViewModel.loading
        ? Container(
            height: double.infinity,
            child: Center(child: CircularProgressIndicator()))
        : Column(children: [Expanded(child: guests), continueButton]);

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.SELECT_GUESTS),
        ),
        body: body);
  }
}
