import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/model/viewModel/tour_sale_view_model.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'select_guests.dart';

class TourSale extends StatelessWidget {
  static final _departureTimeKey = "departure_time_";
  static final _continueButtonKey = Key("continue_button");

  void _onContinue(TourSaleViewModel tourSaleViewModel, BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute<SelectGuests>(
            builder: (BuildContext context) =>
                ChangeNotifierProvider<TourSaleViewModel>(
                  builder: (_) => tourSaleViewModel,
                  child: SelectGuests(),
                )));
  }

  @override
  Widget build(BuildContext context) {
    var tourSaleViewModel = Provider.of<TourSaleViewModel>(context);

    var tourInfo = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: const EdgeInsets.only(bottom: 6, top: 16),
              child: Text(
                tourSaleViewModel.experienceModel.name,
                style: Theme.of(context).textTheme.title,
              )),
          Container(
              margin: const EdgeInsets.only(bottom: 6),
              child: Text(
                "Duration: ${tourSaleViewModel.experienceModel.duration}",
                style: Theme.of(context).textTheme.body1,
              )),
          Text(
            "\$${tourSaleViewModel.experienceModel.adultCost} per adult, " +
                "\$${tourSaleViewModel.experienceModel.childCost} per child",
            style: Theme.of(context).textTheme.subhead,
          )
        ]);

    var times = ListView.builder(
        itemCount: tourSaleViewModel.departures.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              child: RadioListTile(
            title: Text(tourSaleViewModel.departures[index]),
            value: index,
            groupValue: tourSaleViewModel.selectedTimeIndex,
            onChanged: (value) => tourSaleViewModel.selectedTimeIndex = value,
            key: Key("$_departureTimeKey$index"),
          ));
        });

    var departureTimes = SizedBox(
        width: double.infinity,
        child: Container(
            margin: const EdgeInsets.all(24),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      margin: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        Strings.SELECT_DEPARTURE_TIME,
                        style: Theme.of(context).textTheme.title,
                      )),
                  Container(
                      margin: const EdgeInsets.only(bottom: 6),
                      child: Text(
                        Strings.TODAY,
                        style: Theme.of(context).textTheme.body1,
                      )),
                  Expanded(child: times),
                ])));

    var offerConfirm = Container(
        margin: EdgeInsets.all(16),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton(
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: tourSaleViewModel.selectedTime != null
                ? () => _onContinue(tourSaleViewModel, context)
                : null,
            child: Text(Strings.CONTINUE),
            key: _continueButtonKey,
          ),
        ));

    var body = Column(children: [
      Container(
          margin: const EdgeInsets.all(16),
          child: SizedBox(
              width: double.infinity,
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 4.0,
                  child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[tourInfo]))))),
      Expanded(child: departureTimes),
      offerConfirm
    ]);

    return Scaffold(
        appBar: AppBar(
          title: Text(Strings.TOUR_SALES),
        ),
        body: body);
  }
}
