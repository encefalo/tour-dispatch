import 'package:flutter/material.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/utils/strings.dart';

import '../../login/login_manager.dart';
import '../route/login.dart';

class HALDrawer extends StatelessWidget {
  _signOut(BuildContext context) {
    Navigator.pushReplacement(context,
        MaterialPageRoute<Login>(builder: (BuildContext context) => Login()));
  }

  @override
  Widget build(BuildContext context) {
    var user = loginManager.getCurrentUser();

    return ListView(children: [
      UserAccountsDrawerHeader(
          accountName: Text(user.name),
          accountEmail: Text(user.email),
          currentAccountPicture: CircleAvatar(
            backgroundImage: AssetImage(user.imageAsset),
          )),
      ListTile(
          leading: const Icon(Icons.assignment_late),
          onTap: () => guestManager.markAllNotBoarded(context),
          title: const Text(Strings.MARK_ALL_NOT_BOARDED)),
      ListTile(
          leading: const Icon(Icons.settings),
          onTap: null,
          title: Text(
            Strings.SETTINGS,
            style: TextStyle(color: Colors.grey),
          )),
      ListTile(
          leading: const Icon(Icons.feedback),
          onTap: null,
          title: Text(
            Strings.FEEDBACK,
            style: TextStyle(color: Colors.grey),
          )),
      ListTile(
          leading: const Icon(Icons.swap_horiz),
          onTap: () => _signOut(context),
          title: const Text(Strings.SIGN_OUT))
    ]);
  }
}
