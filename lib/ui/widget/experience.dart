import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/model/experience_model.dart';
import 'package:tour_dispatch/model/viewModel/experiences_view_model.dart';
import 'package:tour_dispatch/network/report_sender.dart';
import 'package:tour_dispatch/ui/route/boarding_status.dart';
import 'package:tour_dispatch/utils/dialogs.dart';
import 'package:tour_dispatch/utils/strings.dart';

class ExperiencesList extends StatelessWidget {
  static final listKey = Key('list_key');
  @override
  Widget build(BuildContext context) {
    var experiencesModel = Provider.of<ExperiencesViewModel>(context);

    var experiences = experiencesModel.experiences.values.map((experience) {
      experiencesModel.loading = false;
      return Provider<ExperienceModel>.value(
          value: experience, child: ExperienceCard());
    }).toList();

    if (experiencesModel.loading) {
      return Container(
          height: double.infinity,
          child: Center(child: CircularProgressIndicator()));
    } else {
      return ListView(
        children: experiences,
        key: listKey,
      );
    }
  }
}

class ExperienceCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var experienceModel = Provider.of<ExperienceModel>(context);
    var cardKey = Key('card_key_${experienceModel.id}');
    var buttonExperienceKey = Key('report_key_${experienceModel.id}');

    _sendReport(BuildContext context) async {
      var success = await sendReport();

      if (success) {
        Scaffold.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.blue,
            content: Text(Strings.SEND_REPORT_SUCCESSFUL)));
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.red,
            content: Text(Strings.SEND_REPORT_ERROR)));
      }
    }

    var _onPressed = experienceModel.isEnable
        ? () {
            Navigator.push(
                context,
                MaterialPageRoute<BoardingStatus>(
                    settings: RouteSettings(name: BoardingStatus.ROUTE),
                    builder: (_) => Provider<ExperienceModel>(
                          builder: (_) => experienceModel,
                          child: BoardingStatus(),
                        )));
          }
        : null;

    var _onSendReport = experienceModel.isEnable
        ? () {
            cancelableDialog(
                context,
                Strings.SEND_REPORT,
                [Text(Strings.SEND_REPORT_CONFIRMATION)],
                () => _sendReport(context));
          }
        : null;

    return Padding(
        padding: const EdgeInsets.all(14),
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            elevation: 4.0,
            key: cardKey,
            child: InkWell(
                onTap: _onPressed,
                child: Container(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(10),
                            topRight: const Radius.circular(10)),
                        child: Image.asset(experienceModel.imagePath,
                            fit: BoxFit.cover),
                      ),
                      Container(
                          padding: const EdgeInsets.only(
                              left: 14, top: 8, bottom: 8),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  experienceModel.name,
                                  style: Theme.of(context).textTheme.subtitle,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                    "${experienceModel.reservations} ${Strings.RESERVATIONS_TODAY}",
                                    style: Theme.of(context).textTheme.caption),
                              ])),
                      FlatButton(
                          textColor: Colors.lightBlue,
                          onPressed: _onSendReport,
                          child: Text(Strings.SEND_REPORT),
                          key: buttonExperienceKey)
                    ])))));
  }
}
