import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/boarding/guest_manager.dart';
import 'package:tour_dispatch/model/viewModel/guests_view_model.dart';
import 'package:tour_dispatch/network/report_sender.dart';
import 'package:tour_dispatch/ui/route/guest_information.dart';
import 'package:tour_dispatch/utils/dialogs.dart';
import 'package:tour_dispatch/utils/strings.dart';

var formatter = DateFormat('h:mm a');

class GuestsList extends StatelessWidget {
  final double _appBarHeight = 256;

  _sendReport(BuildContext context) async {
    var success = await sendReport();

    if (success) {
      Scaffold.of(context).showSnackBar(SnackBar(
          backgroundColor: Colors.blue,
          content: Text(Strings.SEND_REPORT_SUCCESSFUL)));
    } else {
      showError(Strings.SEND_REPORT_ERROR, context);
    }
  }

  _onSendReport(BuildContext context) {
    cancelableDialog(context, Strings.SEND_REPORT,
        [Text(Strings.SEND_REPORT_CONFIRMATION)], () => _sendReport(context));
  }

  @override
  Widget build(BuildContext context) {
    var guestsViewModel = Provider.of<GuestsViewModel>(context);

    var appBar = SliverAppBar(
        expandedHeight: _appBarHeight,
        pinned: true,
        forceElevated: true,
        flexibleSpace: FlexibleSpaceBar(
          title: Text("Alaska & The Yukon",
              overflow: TextOverflow.ellipsis,
              softWrap: true,
              textAlign: TextAlign.left),
          background: Image.asset("assets/alaska-banner.jpg",
              fit: BoxFit.cover,
              scale: 0.8,
              alignment: FractionalOffset(0.5, 0.5),
              height: _appBarHeight),
        ),
        actions: <Widget>[
          PopupMenuButton<int>(
              onSelected: (action) => _onSendReport(context),
              itemBuilder: (context) => <PopupMenuItem<int>>[
                    const PopupMenuItem<int>(
                        value: 1, child: const Text(Strings.SEND_REPORT))
                  ])
        ]);

    List<Widget> guests = guestsViewModel.guests.values.map((guest) {
      guestsViewModel.loading = false;
      var _color = guest.boarded ? Colors.blue : Colors.red;
      var boarded = guest.boarded
          ? "${Strings.BOARDED_AT} ${formatter.format(guest.boardingTime)}"
          : "${Strings.BOOKED}";

      return SliverToBoxAdapter(
          child: ListTile(
        title: Text(guest.name, style: TextStyle(fontSize: 20)),
        subtitle: Text(boarded, style: TextStyle(color: _color)),
        onTap: () {
          /* react to the tile being tapped */
          Navigator.push(
              context,
              MaterialPageRoute<GuestInformation>(
                builder: (_) => guestManager.getGuestInfoById(
                    context, guest.id, GuestInformation()),
              ));
        },
      ));
    }).toList();

    var widgets = <Widget>[appBar];

    if (guestsViewModel.loading) {
      widgets.add(SliverToBoxAdapter(
          child: Container(
              margin: const EdgeInsets.only(top: 16.0),
              child: Center(
                child: CircularProgressIndicator(),
              ))));
    } else {
      widgets.addAll(guests);
    }

    return CustomScrollView(slivers: widgets);
  }
}
