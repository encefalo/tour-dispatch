import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/login/login_manager.dart';
import 'package:tour_dispatch/model/viewModel/login_view_model.dart';
import 'package:tour_dispatch/ui/route/home.dart';
import 'package:tour_dispatch/utils/strings.dart';

import '../../utils/validators.dart';

class LoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  static final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  static final emailKey =  Key('email_key');
  static final passwordKey = Key('password_key');
  final _loginViewModel = LoginViewModel();

  @override
  Widget build(BuildContext context) {
    // Binding TextEditControllers to loginViewModel
    _usernameController
        .addListener(() => _loginViewModel.email = _usernameController.text);
    _passwordController
        .addListener(() => _loginViewModel.password = _passwordController.text);

    var widgets = <Widget>[
      Text(Strings.LOGIN_TITLE, style: Theme.of(context).textTheme.subtitle),
      Divider(height: 16, color: Colors.white),
      MultiProvider(
        providers: [
          Provider<String>.value(value: Strings.EMAIL_LABEL),
          Provider<TextEditingController>.value(value: _usernameController),
          Provider<FormFieldValidator<String>>.value(value: emailValidator),
          Provider<Key>.value(value: emailKey),
          Provider<bool>.value(value: false)
        ],
        child: LoginTextField(),
      ),
      Divider(height: 10, color: Colors.white),
      MultiProvider(
        providers: [
          Provider<String>.value(value: Strings.PASSWORD_LABEL),
          Provider<TextEditingController>.value(value: _passwordController),
          Provider<FormFieldValidator<String>>.value(
              value: simpleTextValidator),
          Provider<Key>.value(value: passwordKey),
          Provider<bool>.value(value: true)
        ],
        child: LoginTextField(),
      ),
      SignInButton(),
      SizedBox(
          width: double.infinity,
          child: FlatButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onPressed: null,
              child: Text(Strings.FORGOT_BUTTON)))
    ];

    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5), color: Colors.white),
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 4),
        margin: const EdgeInsets.fromLTRB(16, 110, 16, 16),
        child:
            Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          Form(
              key: _formKey,
              child: ChangeNotifierProvider<LoginViewModel>(
                  builder: (_) => _loginViewModel,
                  child: Column(
                    children: widgets,
                  ))),
        ]));
  }
}

class LoginTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var loginViewModel = Provider.of<LoginViewModel>(context);
    var controller = Provider.of<TextEditingController>(context);
    var validator = Provider.of<FormFieldValidator<String>>(context);
    var label = Provider.of<String>(context);
    var obscureText = Provider.of<bool>(context);
    var key = Provider.of<Key>(context);

    return TextFormField(
      enabled: !loginViewModel.loading,
      controller: controller,
      validator: validator,
      obscureText: obscureText,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(14),
        labelText: label,
        border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
      ),
      key: key,
    );
  }
}

class SignInButton extends StatelessWidget {
  static final signinButton = Key('signin_button');
  @override
  Widget build(BuildContext context) {

    var loginViewModel = Provider.of<LoginViewModel>(context);

    _login() async {
      if (Form.of(context).validate()) {
        // hide keyboard
        SystemChannels.textInput.invokeMethod('TextInput.hide');

        loginViewModel.loading = true;

        bool success = await loginManager.login(
            context, loginViewModel.email, loginViewModel.password);

        if (success) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute<Home>(
                  builder: (BuildContext context) => Home()));
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.red,
              content: Text(Strings.INCORRECT_CREDENTIALS_LABEL)));
        }

        loginViewModel.loading = false;
      }
    }

    if (loginViewModel.loading) {
      return Container(
          height: 110, child: Center(child: CircularProgressIndicator()));
    } else {
      return Container(
          padding: EdgeInsets.only(top: 16),
          child: SizedBox(
            width: double.infinity,
            child: RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () => _login(),
              child: Text(Strings.LOGIN_BUTTON),
            ),
            key: signinButton
          ));
    }
  }
}
