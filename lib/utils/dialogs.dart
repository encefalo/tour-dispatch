import 'package:flutter/material.dart';
import 'package:tour_dispatch/utils/strings.dart';

Future<void> cancelableDialog(BuildContext context, String title,
    List<Widget> content, Future<dynamic> Function() onAccept) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: content,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(Strings.CANCEL),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text(Strings.OK),
            onPressed: () {
              onAccept();
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

showError(String message, BuildContext context) {
  Scaffold.of(context).showSnackBar(
      SnackBar(backgroundColor: Colors.red, content: Text(message)));
}
