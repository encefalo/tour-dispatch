class Strings {
  // App
  static const String APP_NAME = "Tour Dispatch";

  // Dialogs
  static const String CANCEL = "Cancel";
  static const String OK = "OK";

  // Login
  static const String ENTER_A_VALID_NUMBER = "Please enter a valid value";
  static const String ENTER_A_VALID_PASSWORD = "Please enter a valid email address";
  static const String ABOUT = "About this app";
  static const String LOGIN_TITLE = "Please Sign In";
  static const String EMAIL_LABEL = "Email";
  static const String PASSWORD_LABEL = "Password";
  static const String FORGOT_BUTTON = "Forgot your password?";
  static const String INCORRECT_CREDENTIALS_LABEL =
      "Incorrect username or password";
  static const String LOGIN_BUTTON = "Sign In";

  //About
  static const String ABOUT_TITLE = "About Info";

  // Home
  static const String HOME_TITLE = "Experiences";
  static const String RESERVATIONS_TODAY = "reservations for Today";

  // Send Report
  static const String SEND_REPORT = "Send Report";
  static const String SEND_REPORT_CONFIRMATION =
      "Are you sure you want to send the report?";
  static const String SEND_REPORT_SUCCESSFUL = "Report sent successfully";
  static const String SEND_REPORT_ERROR = "Unexpected error sending the report";

  // General Errors
  static const String CAMERA_PERMISSIONS_ERROR =
      "Camera permissions weren't granted";
  static const String BARCODE_SCANNING_ERROR = "Unknown error scanning barcode";

  // Boarding Status
  static const String BOARDED_AT = "boarded at";
  static const String BOOKED = "booked";

  // Drawer
  static const String MARK_ALL_NOT_BOARDED = "Mark everyone as not boarded";
  static const String SETTINGS = "Settings";
  static const String FEEDBACK = "Feedback";
  static const String SIGN_OUT = "Sign Out";

  // Guest Info
  static const String GUEST_INFO = "Guest Info";
  static const String CONFIRM = "Confirm";
  static const String GUEST_NOT_REGISTERED =
      "Guest is not registered in this Tour";
  static const String OFFER_TOUR_TO_SAME_ROOM_GUESTS =
      "You can offer the Tour to this Guest and the Guests in the same stateroom.";
  static const String START_A_SALE = "Start a Sale";
  static const String ALREADY_PROCESSED = "This guest was already processed";
  static const String RESERVATION_APPEARS_AS = "This reservation appears as ";
  static const String CLAIMED = "Claimed ";
  static const String IN_OUR_SYSTEM = "in our system";

  // Guest Information
  static const String GUEST_INFORMATION = "Guest Information";
  static const String CONTACT = "Contact";
  static const String CRUISE_LINE = "Cruise Line";
  static const String EXCURSIONS = "Excursions";

  // TourSale
  static const String TOUR_SALES = "Tour Sales";
  static const String SELECT_DEPARTURE_TIME = "Select a Departure Time";
  static const String TODAY = "Today";
  static const String CONTINUE = "Continue";

  // Select Guests
  static const String SELECT_GUESTS = "Select Guests";
  static const String WHO_IS_GOING = "Who is going to this tour?";
  static const String LIST_OF_GUESTS = "List of Guests in stateroom";

  // Sale Review
  static const String REVIEW = "Review";
  static const String DEPARTURE_TIME = "Departure Time:";
  static const String NUMBER_OF_GUESTS = "Number of Guests:";
  static const String GUESTS = "Guests";
  static const String TOTAL = "Total:";
  static const String CHARGE_TO_ROOM = "Charge will be made to stateroom";
}
