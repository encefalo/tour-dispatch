import 'package:tour_dispatch/utils/strings.dart';

final RegExp _emailRegex = RegExp(r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+');

String simpleTextValidator(String text) {
  if (text.isEmpty) {
    return Strings.ENTER_A_VALID_NUMBER;
  }

  return null;
}

String emailValidator(String email) {
  if (!_emailRegex.hasMatch(email)) {
    return Strings.ENTER_A_VALID_PASSWORD;
  }

  return null;
}
