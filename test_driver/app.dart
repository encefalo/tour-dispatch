import 'package:flutter_driver/driver_extension.dart';
import 'test_main.dart' as app;

void main() {
  enableFlutterDriverExtension();

  app.main();
}