// Imports the Flutter Driver API
import 'dart:convert';
import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:mock_web_server/mock_web_server.dart';
import 'package:test/test.dart';
import 'package:tour_dispatch/model/guest_model.dart';

void main() {
  group("Tour dispatch", () {
    // First, define the Finders. We can use these to locate Widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys
    final emailTextFinder = find.byValueKey("email_key");
    final passwordTextFinder = find.byValueKey("password_key");
    final SerializableFinder singinFinder = find.byValueKey("signin_button");
    final SerializableFinder experienceListFinder = find.byValueKey("list_key");
    final SerializableFinder experienceCardFinder =
        find.byValueKey("card_key_0");
    final SerializableFinder experienceButtonFinder =
        find.byValueKey("report_key_0");

    FlutterDriver driver;
    MockWebServer server;

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();

      List<GuestModel> guests = [
        GuestModel(
            id: 9817230912,
            name: "Isaac Asimov",
            boarded: false,
            boardingTime: null,
            alreadyOnTour: false,
            room: "1208"),
        GuestModel(
            id: 3610223761,
            name: "Robert Chase",
            boarded: false,
            boardingTime: null,
            alreadyOnTour: false,
            room: "1209"),
        GuestModel(
            id: 102010202,
            name: "Elijah Bailey",
            boarded: false,
            boardingTime: null,
            alreadyOnTour: true,
            room: "1210")
      ];

      server = new MockWebServer(port: 8081);
      await server.start();
      MockResponse mockResponse;
      var dispatcher = (HttpRequest request) async {
        switch (request.uri.path) {
          case "/guests":
            mockResponse = new MockResponse()
              ..httpCode = 200
              ..body = jsonEncode(guests);
            break;
          default:
            mockResponse = new MockResponse()..httpCode = 404;
        }

        return mockResponse;
      };
      server.dispatcher = dispatcher;
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (server != null) server.shutdown();
      if (driver != null) driver.close();
    });

    test("Incorrect login", () async {
      Health health = await driver.checkHealth();
      print(health.status);
      await driver.waitFor(emailTextFinder);
      await driver.tap(emailTextFinder);
      await driver.enterText("miguel@globant.com");
      print("Email");

      await driver.waitFor(passwordTextFinder);
      await driver.tap(passwordTextFinder);
      await driver.enterText("1234567");
      print("Password Incorrect");
      await driver.waitFor(singinFinder);
      await driver.tap(singinFinder);
      print("Login");

      await driver.waitFor(find.text("Incorrect username or password"));
      print("Message");
    });

    test("Correct Login", () async {
      Health health = await driver.checkHealth();
      print(health.status);
      await driver.waitFor(emailTextFinder);
      await driver.tap(emailTextFinder);
      await driver.enterText("miguel@globant.com");
      print("Email");

      await driver.waitFor(passwordTextFinder);
      await driver.tap(passwordTextFinder);
      await driver.enterText("123456");
      print("Password");
      await driver.waitFor(singinFinder);
      await driver.tap(singinFinder);
      print("Login");
      await driver.waitFor(experienceListFinder);
      print("Correct login");
    });

    test("Incorrect Experiences", () async {
      print("Start experiences");
      await driver.waitFor(experienceListFinder);
      print("list");

      await driver.waitFor(experienceCardFinder);
      print("Card");

      await driver.waitFor(experienceButtonFinder);
      await driver.tap(experienceButtonFinder);
      print("button");

      await driver.waitFor(find.text("Cancel"));
      await driver.tap(find.text("Cancel"));
      print("cancel");
    });

    test("Experiences correct", () async {
      print("Start experiences");
      await driver.waitFor(experienceListFinder);
      print("list");

      await driver.waitFor(experienceCardFinder);
      print("Card");

      await driver.waitFor(experienceButtonFinder);
      await driver.tap(experienceButtonFinder);
      print("button");

      await driver.waitFor(find.text("OK"));
      await driver.tap(find.text("OK"));
      print("OK");

      await driver.waitFor(find.text("Report sent successfully"));
      print("Send report ok");
    });
  });
}
