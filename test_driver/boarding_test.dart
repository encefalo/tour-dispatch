import 'dart:io';

import "package:flutter_driver/flutter_driver.dart";
import "package:test/test.dart";
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/user_model.dart';
import "package:tour_dispatch/utils/strings.dart";
import 'package:mock_web_server/mock_web_server.dart';
import 'dart:convert';

import 'pre_navigation_test.dart';
import 'test_config.dart';

void main() {

  group("Boarding", () {
    FlutterDriver driver;
    MockWebServer _server;

    setUpAll(() async {
      List<GuestModel> guests = [
          GuestModel(id: 9817230912, name: "Isaac Asimov", boarded: false, boardingTime: null, alreadyOnTour: false, room: "1208"),
          GuestModel(id: 3610223761, name: "Robert Chase", boarded: false, boardingTime: null, alreadyOnTour: false, room: "1209"),
          GuestModel(id: 102010202, name: "Elijah Bailey", boarded: false, boardingTime: null, alreadyOnTour: true, room: "1210")
      ];

      UserModel user = UserModel("Miguel Catiblanco", "miguel@globant.com", "assets/miguel-profile.jpg");

      _server = new MockWebServer(port: 8081);
      await _server.start();
      MockResponse mockResponse;
      var dispatcher = (HttpRequest request) async {
        switch(request.uri.path) {
          case "/login" :
            mockResponse = new MockResponse()
              ..httpCode = 200
              ..body = jsonEncode(user);
            break;
          case "/guests" :
            if(request.method == "PUT") {
              guests.removeWhere((guest) => guest.id == 102010202);
              guests.add(GuestModel(id: 102010202, name: "Elijah Bailey", boarded: true, boardingTime: DateTime.parse("2019-06-17 15:57:40.061099"), alreadyOnTour: true, room: "1209"));
              mockResponse = new MockResponse()
                ..httpCode = 200;
            } else {
              mockResponse = new MockResponse()
                ..httpCode = 200
                ..body = jsonEncode(guests);
            }
            break;
          default:
            mockResponse = new MockResponse()..httpCode = 404;
        }

        return mockResponse;
      };
      _server.dispatcher = dispatcher;

      print("Setting up...");
      TestConfig.grantPermission(TestConfig.ANDROID_PERMISSION_CAMERA);
      driver = await FlutterDriver.connect();
      print("Set up done");
    });

    tearDownAll(() async {
      if(_server != null) _server.shutdown();
      if (driver != null) driver.close();
      // TODO Clear guests registered and boarded on tests
    });

    test("Openning Bar Code scanner", () async {
      print("Scanning BarCode...");
      var experienceCardFinder = find.byValueKey("card_key_0");
      var scanButtonFinder = find.byValueKey("scan_button_key");

      await PreNavigationTest.doLogin(driver);

      await driver.waitFor(experienceCardFinder);
      await driver.tap(experienceCardFinder);

      await driver.waitFor(scanButtonFinder);
      await driver.tap(scanButtonFinder);

      await driver.waitFor(find.text(Strings.GUEST_INFO));
      print("BarCode Scanned");
    });

    /** To perform these tests a few pre conditions have to been met:
     *  1. Elijah Bailey should be registered but not boarded yet
     */
    test("Guest Info", () async {
      // Not registered guest
      print("Starting Guest Info booked guest...");
      var confirmButtonFinder = find.byValueKey("confirmed_button_key");
      await driver.waitFor(find.text("Elijah Bailey"));
      await driver.waitFor(find.text("Alaska & The Yukon"));
      await driver.waitFor(find.text("Today"));
      await driver.tap(confirmButtonFinder);
    });

    test("Validate guests", () async {
      print("Validating guests...");
      await driver.waitFor(find.text("Alaska & The Yukon"));
      await driver.waitFor(find.text("Elijah Bailey"));
      await driver.waitFor(find.text("boarded at 3:57 PM"));
      print("Guests validated");
    });
  });
}
