import 'dart:io';

import "package:flutter_driver/flutter_driver.dart";
import "package:test/test.dart";
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/user_model.dart';
import "package:tour_dispatch/utils/strings.dart";
import 'package:mock_web_server/mock_web_server.dart';
import 'dart:convert';

import 'pre_navigation_test.dart';
import 'test_config.dart';

void main() {

  group("Guest Information", () {
    FlutterDriver driver;
    MockWebServer _server;

    setUpAll(() async {
      List<GuestModel> guests = [
          GuestModel(id: 9817230912, name: "Isaac Asimov", boarded: false, boardingTime: null, alreadyOnTour: false),
          GuestModel(id: 3610223761, name: "Robert Chase", boarded: false, boardingTime: null, alreadyOnTour: false),
          GuestModel(id: 102010202, name: "Elijah Bailey", boarded: true, boardingTime: DateTime.parse("2019-06-17 15:57:40.061099"), alreadyOnTour: true)
      ];

      UserModel user = UserModel("Miguel Catiblanco", "miguel@globant.com", "assets/miguel-profile.jpg");

      _server = new MockWebServer(port: 8081);
      await _server.start();
      MockResponse mockResponse;
      var dispatcher = (HttpRequest request) async {
        switch(request.uri.path) {
          case "/login" :
            mockResponse = new MockResponse()
              ..httpCode = 200
              ..body = jsonEncode(user);
            break;
          case "/guests" :
            if(request.method == "PUT") {
              guests.removeWhere((guest) => guest.id == 102010202);
              guests.add(GuestModel(id: 102010202, name: "Elijah Bailey", boarded: true, boardingTime: DateTime.parse("2019-06-17 15:57:40.061099"), alreadyOnTour: true));
              mockResponse = new MockResponse()
                ..httpCode = 200;
            } else {
              mockResponse = new MockResponse()
                ..httpCode = 200
                ..body = jsonEncode(guests);
            }
            break;
          default:
            mockResponse = new MockResponse()..httpCode = 404;
        }

        return mockResponse;
      };
      _server.dispatcher = dispatcher;

      print("Setting up...");
      TestConfig.grantPermission(TestConfig.ANDROID_PERMISSION_CAMERA);
      driver = await FlutterDriver.connect();
      print("Set up done");
    });

    tearDownAll(() async {
      if(_server != null) _server.shutdown();
      if (driver != null) driver.close();
      // TODO Clear guests registered and boarded on tests
    });

    test("Guest information", () async {
      print("Obtaining guest information");
      var experienceCardFinder = find.byValueKey("card_key_0");
      var guestFinder = find.text("Elijah Bailey");

      await PreNavigationTest.doLogin(driver);

      await driver.waitFor(experienceCardFinder);
      await driver.tap(experienceCardFinder);

      await driver.waitFor(guestFinder);
      await driver.tap(guestFinder);

      await driver.waitFor(find.text(Strings.GUEST_INFORMATION));
      await driver.waitFor(find.text("Elijah Bailey"));
      await driver.waitFor(find.text("Contact"));
      await driver.waitFor(find.text("555 444 1111"));
      await driver.waitFor(find.text("Cruise Line"));
      await driver.waitFor(find.text("Holland American Line"));
      await driver.waitFor(find.text("Excursions"));
      await driver.waitFor(find.text("Juneau Culinary Walk"));
      await driver.waitFor(find.text("Boarded at 11:25AM"));
      await driver.waitFor(find.text("Bear Watching and Wildlife Viewing Adventure"));
      await driver.waitFor(find.text("Boarded at 2:12PM"));
      await driver.waitFor(find.text("Dog Sleding on Mendenhall Glacier"));
      await driver.waitFor(find.text("Reserved"));
      print("Guest information obtained");
    });
  });
}
