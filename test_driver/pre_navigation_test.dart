import 'package:flutter_driver/flutter_driver.dart';

class PreNavigationTest {
  static doLogin(FlutterDriver driver) async {
    print("Sign in...");
    var emailTextFinder = find.byValueKey("email_key");
    var passwordTextFinder = find.byValueKey("password_key");
    var singInFinder = find.byValueKey("signin_button");
    var experienceListFinder = find.byValueKey("list_key");

    Health health = await driver.checkHealth();
    print(health.status);

    await driver.waitFor(emailTextFinder);
    await driver.tap(emailTextFinder);
    await driver.enterText("miguel@globant.com");

    await driver.waitFor(passwordTextFinder);
    await driver.tap(passwordTextFinder);
    await driver.enterText("123456");

    await driver.waitFor(singInFinder);
    await driver.tap(singInFinder);

    await driver.waitFor(experienceListFinder);
    print("Signed in");
  }
}
