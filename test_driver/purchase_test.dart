import 'dart:convert';
import 'dart:io';

import "package:flutter_driver/flutter_driver.dart";
import 'package:mock_web_server/mock_web_server.dart';
import "package:test/test.dart";
import 'package:tour_dispatch/model/guest_model.dart';
import 'package:tour_dispatch/model/user_model.dart';
import "package:tour_dispatch/utils/strings.dart";

import 'pre_navigation_test.dart';
import 'test_config.dart';

void main() {
  group("Purchase", () {
    FlutterDriver driver;
    MockWebServer _server;

    setUpAll(() async {
      List<GuestModel> guests = [
          GuestModel(id: 9817230912, name: "Isaac Asimov", boarded: false, boardingTime: null, alreadyOnTour: false),
          GuestModel(id: 3610223761, name: "Robert Chase", boarded: false, boardingTime: null, alreadyOnTour: false),
      ];

      UserModel user = UserModel("Miguel Catiblanco", "miguel@globant.com", "assets/miguel-profile.jpg");

      _server = new MockWebServer(port: 8081);
      await _server.start();
      MockResponse mockResponse;
      var dispatcher = (HttpRequest request) async {
        switch(request.uri.path) {
          case "/login" :
            mockResponse = new MockResponse()
              ..httpCode = 200
              ..body = json.encode(user);
            break;
          case "/guests" :
            if(request.method == "POST") {
              guests.add(GuestModel(id: 102010202, name: "Elijah Bailey", boarded: true, boardingTime: new DateTime.now(), alreadyOnTour: true));
              mockResponse = new MockResponse()
                ..httpCode = 200;
            } else {
              mockResponse = new MockResponse()
                ..httpCode = 200
                ..body = jsonEncode(guests);
            }
            break;
          default:
            mockResponse = new MockResponse()..httpCode = 404;
        }

        return mockResponse;
      };
      _server.dispatcher = dispatcher;

      print("Setting up...");
      TestConfig.grantPermission(TestConfig.ANDROID_PERMISSION_CAMERA);
      driver = await FlutterDriver.connect();
      print("Set up done");
    });

    tearDownAll(() async {
      if(_server != null) _server.shutdown();
      if (driver != null) driver.close();
      // TODO Clear guests registered and boarded on tests
    });

    test("Openning Bar Code scanner", () async {
      print("Scanning BarCode...");
      var experienceCardFinder = find.byValueKey("card_key_0");
      var scanButtonFinder = find.byValueKey("scan_button_key");

      await PreNavigationTest.doLogin(driver);

      await driver.waitFor(experienceCardFinder);
      await driver.tap(experienceCardFinder);

      await driver.waitFor(scanButtonFinder);
      await driver.tap(scanButtonFinder);

      await driver.waitFor(find.text(Strings.GUEST_INFO));
      print("BarCode Scanned");
    });

    /** To perform these tests a few pre conditions have to been met:
     *  1. Elijah Bailey shouldn"t be registered on tour
     */

    test("Guest Info", () async {
      // Not registered guest
      print("Starting Guest Info sale to not registered guest...");
      var startSaleButtonFinder = find.byValueKey("start_sale_button_key");

      await driver.waitFor(find.text("Elijah Bailey"));
      await driver.waitFor(find.text(Strings.GUEST_NOT_REGISTERED));
      await driver.waitFor(find.text(Strings.OFFER_TOUR_TO_SAME_ROOM_GUESTS));

      await driver.waitFor(startSaleButtonFinder);
      await driver.tap(startSaleButtonFinder);

      await driver.waitFor(find.text(Strings.TOUR_SALES));
      print("Guest Info sale started to not registered guest");
    });

    test("Tour Sales", () async {
      print("Choosing time...");
      var departureTime1100Finder = find.byValueKey("departure_time_1");
      var continueButtonFinder = find.byValueKey("continue_button");

      await driver.waitFor(find.text("Alaska & The Yukon"));

      await driver.waitFor(departureTime1100Finder);
      await driver.tap(departureTime1100Finder);
      await driver.waitFor(continueButtonFinder);
      await driver.tap(continueButtonFinder);

      await driver.waitFor(find.text(Strings.SELECT_GUESTS));
      print("Choosed time");
    });

    test("Select Guests", () async {
      print("Choosing guests...");
      var guestCheckBoxFinder = find.byValueKey("guest_checkbox_3");
      var continueButtonFinder = find.byValueKey("continue_button");

      await driver.waitFor(guestCheckBoxFinder);
      await driver.tap(guestCheckBoxFinder);

      await driver.waitFor(continueButtonFinder);
      await driver.tap(continueButtonFinder);

      await driver.waitFor(find.text(Strings.REVIEW));
      print("Choosed guests...");
    });

    test("Review", () async {
      print("Reviwing sale...");
      var confirmButtonFinder = find.byValueKey("confirm_button");

      await driver.waitFor(find.text("Alaska & The Yukon"));
      await driver.waitFor(find.text("${Strings.DEPARTURE_TIME} 11:00 AM"));
      await driver.waitFor(find.text("Elijah Bailey"));

      await driver.tap(confirmButtonFinder);
      print("Sale reviewed...");
    });

    test("Validate guests", () async {
      print("Validating guests...");
      await driver.waitFor(find.text("Alaska & The Yukon"));
      await driver.waitFor(find.text("Elijah Bailey"));
      print("Guests validated");
    });
  });
}
