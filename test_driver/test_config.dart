import 'dart:io';

import 'package:path/path.dart';

class TestConfig {
  static const String ANDROID_PERMISSION_CAMERA = 'android.permission.CAMERA';

  static grantPermission(String resource) async {
    final Map<String, String> envVars = Platform.environment;
    final adbPath = join(
      envVars['ANDROID_HOME'],
      'platform-tools',
      Platform.isWindows ? 'adb.exe' : 'adb',
    );
    print(adbPath);

    await Process.run(adbPath,
        ['shell', 'pm', 'grant', 'com.example.tourdispatch', resource]);
  }
}
