import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tour_dispatch/ui/route/login.dart';
import 'package:tour_dispatch/utils/strings.dart';

import 'test_provider_setup.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiProvider(
        providers: providers,
        child: MaterialApp(
          title: "${Strings.APP_NAME} test",
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Login(),
        ));
  }
}
