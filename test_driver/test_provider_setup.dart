// provider_setup.dart
import 'package:provider/provider.dart';
import 'package:tour_dispatch/app_config.dart';
import 'package:tour_dispatch/db/db_manager.dart';

List<SingleChildCloneableWidget> providers = [
  ...independentServices,
  ...dependentServices,
  ...uiConsumableProviders,
];

List<SingleChildCloneableWidget> independentServices = [
  Provider.value(
      value: AppConfig(flavorName: "test", apiBaseUrl: "10.0.2.2:8081")),
  Provider<DatabaseManager>.value(value: FDatabaseManager.internal()),
];

List<SingleChildCloneableWidget> dependentServices = [];

List<SingleChildCloneableWidget> uiConsumableProviders = [];
